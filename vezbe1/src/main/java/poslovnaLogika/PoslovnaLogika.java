package poslovnaLogika;


import repozitorijumArtikala.RepozitorijumArtikala;
import java.util.List;

import artikal.Artikal;
public class PoslovnaLogika {

	private RepozitorijumArtikala repozitorijumArtikala;
	public PoslovnaLogika() {
		
	}
	public PoslovnaLogika(RepozitorijumArtikala repozitorijumArtikala) {
		
		super();
		this.repozitorijumArtikala=repozitorijumArtikala;
		
	}
	public RepozitorijumArtikala getRepozitorijumArtikala() {
		return repozitorijumArtikala;
	}
	public void setRepozitorijumArtikala(RepozitorijumArtikala repozitorijumArtikala) {
		this.repozitorijumArtikala = repozitorijumArtikala;
	}
	
	public List<Artikal> findAll() {
		return repozitorijumArtikala.findAll();
	}
	
	public Artikal findOne(int id) {
		return repozitorijumArtikala.findOneById(id);
	}
	
	public void save(Artikal artikal) {
		repozitorijumArtikala.save(artikal);
	}
	
	public void delete(int id) {
		repozitorijumArtikala.delete(id);
	}
	
	public void delete(Artikal artikal) {
		repozitorijumArtikala.delete(artikal);
	}
	
	public void akcija(String naziv, double cena, String opis,int id,double procenat) {
		Artikal pocetnaCena = repozitorijumArtikala.findOneByCena(cena);
		Artikal smanjenaCena = repozitorijumArtikala.findOneById(id);
		pocetnaCena.setCena(pocetnaCena.getCena()*procenat);
		smanjenaCena.setCena(smanjenaCena.getCena()-pocetnaCena.getCena());
		repozitorijumArtikala.save(pocetnaCena);
		repozitorijumArtikala.save(smanjenaCena);
	}
	
	
	

}
