package artikal;



public class Artikal {
	private String naziv;
	private double cena;
	private String opisom;
	private int id;
	
	
	public Artikal(String naziv, double cena, String opisom, int id) {
		super();
		this.naziv = naziv;
		this.cena = cena;
		this.opisom = opisom;
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public String getOpisom() {
		return opisom;
	}
	public void setOpisom(String opisom) {
		this.opisom = opisom;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}


	

}
