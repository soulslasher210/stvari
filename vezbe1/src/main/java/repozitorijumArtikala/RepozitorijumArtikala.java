package repozitorijumArtikala;

import java.util.ArrayList;
import java.util.List;

import artikal.Artikal;
import java.util.Iterator;


public class RepozitorijumArtikala {
	private ArrayList<Artikal>p;

	public RepozitorijumArtikala() {
		super();
		p=new ArrayList<Artikal>();
		p.add(new Artikal("Jabuke",120,"Ajdara",1));
		p.add(new Artikal("Limun", 150, "Tropski", 2));
		p.add(new Artikal("Paradajz", 95, "Volovko srce", 3));
	}
	public List<Artikal>findAll(){
		return p;
		
	}
	public Artikal findOneById(int id) {
		for(Artikal r : p) {
			if(r.getId()==id) {
				return r;
			}
		}
		
		return null;
	}
	
	public Artikal findOneByCena(double cena) {
		for(Artikal r : p) {
			if(r.getCena()==cena) {
				return r;
			}
		}
		
		return null;
	}
	
	public void save(Artikal Artikal) {
		Artikal r = findOneById(Artikal.getId());
		
		if(r == null) {
			p.add(Artikal);
		} else {
			r.setNaziv(r.getNaziv());
			r.setCena(r.getCena());
			r.setOpisom(r.getOpisom());
		}
	}
	public void delete(int i) {
		Iterator<Artikal> iterator = p.iterator();
		while(iterator.hasNext()) {
			Artikal r = iterator.next();
			if(r.getId()==i) {
				iterator.remove();
				return;
			}
		}
	}
	
	public void delete(Artikal artikal) {
		delete(artikal.getId());
	}
	
	
	

}
